package com.bbva.mbnx.batch;

import java.util.List;

import org.springframework.batch.item.ItemWriter;

public class CustomWriter implements ItemWriter<Evento>{

	@Override
	public void write(List<? extends Evento> items)throws Exception{
		for(Evento evento:items){
			System.out.println(evento.toString());
		}
	}
}
