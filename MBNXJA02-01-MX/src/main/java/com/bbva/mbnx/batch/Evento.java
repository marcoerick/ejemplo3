package com.bbva.mbnx.batch;

public class Evento {
	
	private String numevento;
	private String tipomsj;
	private String codplant;
	private String msjenviado;
	private String fechalta;
	private String usuarioalta;
	private String fechultmod;
	private String statusdesc;
	private String terminal;
	private String userultmod;
	private String fhoramod;
	
	public String getNumevento() {
		return numevento;
	}
	public void setNumevento(String numevento) {
		this.numevento = numevento;
	}
	public String getTipomsj() {
		return tipomsj;
	}
	public void setTipomsj(String tipomsj) {
		this.tipomsj = tipomsj;
	}
	public String getCodplant() {
		return codplant;
	}
	public void setCodplant(String codplant) {
		this.codplant = codplant;
	}
	public String getMsjenviado() {
		return msjenviado;
	}
	public void setMsjenviado(String msjenviado) {
		this.msjenviado = msjenviado;
	}
	public String getFechalta() {
		return fechalta;
	}
	public void setFechalta(String fechalta) {
		this.fechalta = fechalta;
	}
	public String getUsuarioalta() {
		return usuarioalta;
	}
	public void setUsuarioalta(String usuarioalta) {
		this.usuarioalta = usuarioalta;
	}
	public String getFechultmod() {
		return fechultmod;
	}
	public void setFechultmod(String fechultmod) {
		this.fechultmod = fechultmod;
	}
	public String getStatusdesc() {
		return statusdesc;
	}
	public void setStatusdesc(String statusdesc) {
		this.statusdesc = statusdesc;
	}
	public String getTerminal() {
		return terminal;
	}
	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}
	public String getUserultmod() {
		return userultmod;
	}
	public void setUserultmod(String userultmod) {
		this.userultmod = userultmod;
	}
	public String getFhoramod() {
		return fhoramod;
	}
	public void setFhoramod(String fhoramod) {
		this.fhoramod = fhoramod;
	}
	
	@Override
	public String toString() {
		return "Evento [numevento=" + numevento + ", tipomsj=" + tipomsj + ", codplant=" + codplant + ", msjenviado="
				+ msjenviado + ", fechalta=" + fechalta + ", usuarioalta=" + usuarioalta + ", fechultmod=" + fechultmod
				+ ", statusdesc=" + statusdesc + ", terminal=" + terminal + ", userultmod=" + userultmod + ", fhoramod="
				+ fhoramod + "]";
	}


}
